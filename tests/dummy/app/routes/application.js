import Route from '@ember/routing/route';

export default Route.extend({
  model() {
    return {
      'items': [
        {
          'dc:creator': 'Brandon',
          'pubDate': '12/07/2018',
          'content:encoded': 'Design, illustrate, and produce your visual learning needs. From designs in print and web graphics, to simulated virtual environments and audio/video storytelling, we create a unique place for the students to engage and learn.',
          'title': 'Multimedia',
          'link': 'http://www.mosaiclearning.com/platforms/'
        },
        {
          'dc:creator': 'Willow',
          'pubDate': '12/07/2018',
          'content:encoded': '<p><a><img src="http://cdn0.tnwcdn.com/wp-content/blogs.dir/1/files/2011/07/mobile-apps-2.jpeg" /></a> We maximize productivity by bringing the power of learning to the palm of your hand. From the simple to the complex, we deliver a complete framework that fully integrates into your existing infrastructure.</p>',
          'title': 'Mobile Applications',
          'link': 'http://www.mosaiclearning.com/platforms/'
        },
        {
          'dc:creator': 'Andrew',
          'pubDate': '12/07/2018',
          'content:encoded': '<p><a><img src="https://cdn-images-1.medium.com/max/1600/1*1dkUNOPVLoks_RVf-5pEsA.png" /></a> Develop comprehensive and flexible solutions that enable you to meet your learning and business challenges head-on. Our custom applications are built to increase productivity and performance.</p>',
          'title': 'Custom App Development',
          'link': 'http://www.mosaiclearning.com/platforms/'
        }
      ]
    }
  }

});
