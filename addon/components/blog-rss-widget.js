import Component from '@ember/component';

export default Component.extend({
  length: 150,
  limit: 3,
  moreText: 'Continue reading',
  moreDisabled: false,
  emptyText: 'There is no content to display.',
  title: 'Blog Feed',
  subtitle: 'Latest posts from our blog.',
  dateFormat: 'MM/DD/YYYY',

  /**
   * Leave blank to replace all insecure links (http://*) with (https://*), otherwise
   * specify the target URL to secure.
   */
  targetToSecure: ''
});
